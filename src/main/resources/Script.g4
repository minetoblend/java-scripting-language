grammar Script;

compilationUnit
    :   importStatements block
    ;

block
    :   statement*
    ;

statement
    :   methodInvokation
    |   variableCreation
    |   variableAssignment
    ;

variableCreation
    :   'var' Identifier #varDefNoInit
    |   'var' Identifier '=' expression #varDef
    ;


variableAssignment
    :   Identifier '=' expression
    ;

methodInvokation
    :   methodName '(' argumentList? ')' #simpleInvokation
    |   expressionName '.' methodName '(' argumentList? ')' #expressionInvokation
    |   typeName '.' methodName '(' argumentList? ')' #typeNameInvocation
    ;

typeName
    :   Identifier #simpleTypeName
    |	packageOrTypeName '.' Identifier #complexTypename
    ;

packageOrTypeName
	:	Identifier
	|	packageOrTypeName '.' Identifier
	;

expressionName
    :   Identifier
    ;

argumentList
    :   expression (',' expression)?
    ;

expression
    :   addition
    ;

addition
    :   a=primitive ('+' b=addition)?
    ;

primitive
    :   StringLiteral #stringLiteral
    ;

StringLiteral
	:	'"' StringCharacters? '"'
	;

fragment
StringCharacters
	:	StringCharacter+
	;

fragment
StringCharacter
	:	~["\\\r\n]
	|	EscapeSequence
	;

// §3.10.6 Escape Sequences for Character and String Literals

fragment
EscapeSequence
	:	'\\' [btnfr"'\\]
	;

methodName
    :   Identifier
    ;

importStatements
    :   importStatement*
    ;


importStatement
    :   IMPORT classPath ';'?
    ;

classPath
    :   Identifier ('.' Identifier)+
    ;



json
   : value
   ;

obj
   : '{' pair (',' pair)* '}'
   | '{' '}'
   ;

pair
   : STRING ':' value
   ;

array
   : '[' value (',' value)* ']'
   | '[' ']'
   ;

value
   : STRING
   | NUMBER
   | obj
   | array
   | 'true'
   | 'false'
   | 'null'
   ;


IMPORT
    :   'import'
    ;

Identifier
    :   Letter (LetterOrDigit)*
    ;

STRING
   : '"' (ESC | SAFECODEPOINT)* '"'
   ;


fragment ESC
   : '\\' (["\\/bfnrt] | UNICODE)
   ;
fragment UNICODE
   : 'u' HEX HEX HEX HEX
   ;
fragment HEX
   : [0-9a-fA-F]
   ;
fragment SAFECODEPOINT
   : ~ ["\\\u0000-\u001F]
   ;


NUMBER
   : '-'? INT ('.' [0-9] +)? EXP?
   ;


fragment INT
   : '0' | [1-9] [0-9]*
   ;

// no leading zeros

fragment EXP
   : [Ee] [+\-]? INT
   ;

// \- since - means "range" inside [...]

fragment
Letter
	:	[a-zA-Z$_] // these are the "java letters" below 0x7F
	|	// covers all characters above 0x7F which are not a surrogate
		~[\u0000-\u007F\uD800-\uDBFF]
		{Character.isJavaIdentifierStart(_input.LA(-1))}?
	|	// covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
		[\uD800-\uDBFF] [\uDC00-\uDFFF]
		{Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
	;

fragment
LetterOrDigit
	:	[a-zA-Z0-9$_] // these are the "java letters or digits" below 0x7F
	|	// covers all characters above 0x7F which are not a surrogate
		~[\u0000-\u007F\uD800-\uDBFF]
		{Character.isJavaIdentifierPart(_input.LA(-1))}?
	|	// covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
		[\uD800-\uDBFF] [\uDC00-\uDFFF]
		{Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
	;


WS
   : [ \t\n\r] + -> skip
   ;


COMMENT
    :   '/*' .*? '*/' -> channel(HIDDEN)
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> channel(HIDDEN)
    ;
