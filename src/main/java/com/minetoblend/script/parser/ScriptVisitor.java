// Generated from C:/Users/Marvin/IdeaProjects/script/src/main/resources\Script.g4 by ANTLR 4.7
package com.minetoblend.script.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ScriptParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ScriptVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ScriptParser#compilationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompilationUnit(ScriptParser.CompilationUnitContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(ScriptParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(ScriptParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varDefNoInit}
	 * labeled alternative in {@link ScriptParser#variableCreation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDefNoInit(ScriptParser.VarDefNoInitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varDef}
	 * labeled alternative in {@link ScriptParser#variableCreation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDef(ScriptParser.VarDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#variableAssignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableAssignment(ScriptParser.VariableAssignmentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code simpleInvokation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleInvokation(ScriptParser.SimpleInvokationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expressionInvokation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionInvokation(ScriptParser.ExpressionInvokationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeNameInvocation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeNameInvocation(ScriptParser.TypeNameInvocationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code simpleTypeName}
	 * labeled alternative in {@link ScriptParser#typeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleTypeName(ScriptParser.SimpleTypeNameContext ctx);
	/**
	 * Visit a parse tree produced by the {@code complexTypename}
	 * labeled alternative in {@link ScriptParser#typeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplexTypename(ScriptParser.ComplexTypenameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#packageOrTypeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPackageOrTypeName(ScriptParser.PackageOrTypeNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#expressionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionName(ScriptParser.ExpressionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#argumentList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgumentList(ScriptParser.ArgumentListContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(ScriptParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#addition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddition(ScriptParser.AdditionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringLiteral}
	 * labeled alternative in {@link ScriptParser#primitive}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLiteral(ScriptParser.StringLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#methodName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodName(ScriptParser.MethodNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#importStatements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportStatements(ScriptParser.ImportStatementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#importStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportStatement(ScriptParser.ImportStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#classPath}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassPath(ScriptParser.ClassPathContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJson(ScriptParser.JsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#obj}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObj(ScriptParser.ObjContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#pair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair(ScriptParser.PairContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray(ScriptParser.ArrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link ScriptParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(ScriptParser.ValueContext ctx);
}