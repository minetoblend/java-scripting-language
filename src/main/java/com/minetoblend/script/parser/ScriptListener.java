// Generated from C:/Users/Marvin/IdeaProjects/script/src/main/resources\Script.g4 by ANTLR 4.7
package com.minetoblend.script.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ScriptParser}.
 */
public interface ScriptListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ScriptParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void enterCompilationUnit(ScriptParser.CompilationUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void exitCompilationUnit(ScriptParser.CompilationUnitContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(ScriptParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(ScriptParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(ScriptParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(ScriptParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varDefNoInit}
	 * labeled alternative in {@link ScriptParser#variableCreation}.
	 * @param ctx the parse tree
	 */
	void enterVarDefNoInit(ScriptParser.VarDefNoInitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varDefNoInit}
	 * labeled alternative in {@link ScriptParser#variableCreation}.
	 * @param ctx the parse tree
	 */
	void exitVarDefNoInit(ScriptParser.VarDefNoInitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varDef}
	 * labeled alternative in {@link ScriptParser#variableCreation}.
	 * @param ctx the parse tree
	 */
	void enterVarDef(ScriptParser.VarDefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varDef}
	 * labeled alternative in {@link ScriptParser#variableCreation}.
	 * @param ctx the parse tree
	 */
	void exitVarDef(ScriptParser.VarDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#variableAssignment}.
	 * @param ctx the parse tree
	 */
	void enterVariableAssignment(ScriptParser.VariableAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#variableAssignment}.
	 * @param ctx the parse tree
	 */
	void exitVariableAssignment(ScriptParser.VariableAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleInvokation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 */
	void enterSimpleInvokation(ScriptParser.SimpleInvokationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleInvokation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 */
	void exitSimpleInvokation(ScriptParser.SimpleInvokationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expressionInvokation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 */
	void enterExpressionInvokation(ScriptParser.ExpressionInvokationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expressionInvokation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 */
	void exitExpressionInvokation(ScriptParser.ExpressionInvokationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeNameInvocation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 */
	void enterTypeNameInvocation(ScriptParser.TypeNameInvocationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeNameInvocation}
	 * labeled alternative in {@link ScriptParser#methodInvokation}.
	 * @param ctx the parse tree
	 */
	void exitTypeNameInvocation(ScriptParser.TypeNameInvocationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code simpleTypeName}
	 * labeled alternative in {@link ScriptParser#typeName}.
	 * @param ctx the parse tree
	 */
	void enterSimpleTypeName(ScriptParser.SimpleTypeNameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code simpleTypeName}
	 * labeled alternative in {@link ScriptParser#typeName}.
	 * @param ctx the parse tree
	 */
	void exitSimpleTypeName(ScriptParser.SimpleTypeNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code complexTypename}
	 * labeled alternative in {@link ScriptParser#typeName}.
	 * @param ctx the parse tree
	 */
	void enterComplexTypename(ScriptParser.ComplexTypenameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code complexTypename}
	 * labeled alternative in {@link ScriptParser#typeName}.
	 * @param ctx the parse tree
	 */
	void exitComplexTypename(ScriptParser.ComplexTypenameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#packageOrTypeName}.
	 * @param ctx the parse tree
	 */
	void enterPackageOrTypeName(ScriptParser.PackageOrTypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#packageOrTypeName}.
	 * @param ctx the parse tree
	 */
	void exitPackageOrTypeName(ScriptParser.PackageOrTypeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#expressionName}.
	 * @param ctx the parse tree
	 */
	void enterExpressionName(ScriptParser.ExpressionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#expressionName}.
	 * @param ctx the parse tree
	 */
	void exitExpressionName(ScriptParser.ExpressionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#argumentList}.
	 * @param ctx the parse tree
	 */
	void enterArgumentList(ScriptParser.ArgumentListContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#argumentList}.
	 * @param ctx the parse tree
	 */
	void exitArgumentList(ScriptParser.ArgumentListContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(ScriptParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(ScriptParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#addition}.
	 * @param ctx the parse tree
	 */
	void enterAddition(ScriptParser.AdditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#addition}.
	 * @param ctx the parse tree
	 */
	void exitAddition(ScriptParser.AdditionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringLiteral}
	 * labeled alternative in {@link ScriptParser#primitive}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteral(ScriptParser.StringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringLiteral}
	 * labeled alternative in {@link ScriptParser#primitive}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteral(ScriptParser.StringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#methodName}.
	 * @param ctx the parse tree
	 */
	void enterMethodName(ScriptParser.MethodNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#methodName}.
	 * @param ctx the parse tree
	 */
	void exitMethodName(ScriptParser.MethodNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#importStatements}.
	 * @param ctx the parse tree
	 */
	void enterImportStatements(ScriptParser.ImportStatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#importStatements}.
	 * @param ctx the parse tree
	 */
	void exitImportStatements(ScriptParser.ImportStatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void enterImportStatement(ScriptParser.ImportStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void exitImportStatement(ScriptParser.ImportStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#classPath}.
	 * @param ctx the parse tree
	 */
	void enterClassPath(ScriptParser.ClassPathContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#classPath}.
	 * @param ctx the parse tree
	 */
	void exitClassPath(ScriptParser.ClassPathContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#json}.
	 * @param ctx the parse tree
	 */
	void enterJson(ScriptParser.JsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#json}.
	 * @param ctx the parse tree
	 */
	void exitJson(ScriptParser.JsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#obj}.
	 * @param ctx the parse tree
	 */
	void enterObj(ScriptParser.ObjContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#obj}.
	 * @param ctx the parse tree
	 */
	void exitObj(ScriptParser.ObjContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#pair}.
	 * @param ctx the parse tree
	 */
	void enterPair(ScriptParser.PairContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#pair}.
	 * @param ctx the parse tree
	 */
	void exitPair(ScriptParser.PairContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#array}.
	 * @param ctx the parse tree
	 */
	void enterArray(ScriptParser.ArrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#array}.
	 * @param ctx the parse tree
	 */
	void exitArray(ScriptParser.ArrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScriptParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(ScriptParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScriptParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(ScriptParser.ValueContext ctx);
}