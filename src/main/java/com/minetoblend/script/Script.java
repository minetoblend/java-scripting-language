package com.minetoblend.script;

import com.minetoblend.script.compiler.ScriptCompiler;
import com.minetoblend.script.kernel.ScriptInstruction;
import com.minetoblend.script.kernel.ScriptMethod;

import java.io.IOException;
import java.util.*;

public class Script {

    Map<String, Object> variables = new HashMap<>();

    List<ScriptInstruction> commands = new ArrayList<>();

    public Script(String code) {
        new ScriptCompiler().compile(code, this);
    }

    public static void main(String[] args) {
        String s = null;
        try {
            s = new Scanner(Script.class.getClassLoader().getResource("test.script").openStream(), "UTF-8").useDelimiter("\\A").next();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Script script = new Script(s);
    }

    Object invokeFunction(String function, Object... args) {
        Object o = variables.get(function);
        if (o instanceof ScriptMethod)
            return ((ScriptMethod) o).invoke(null, args);
        throw new IllegalArgumentException("'" + function + "'is not a function");
    }

    public void clear() {
        variables.clear();
    }

    public void put(String name, Object object) {
        variables.put(name, object);
    }
}
