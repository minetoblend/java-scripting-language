package com.minetoblend.script.kernel;

public interface Settable<O> {

    void set(Object o);

}
