package com.minetoblend.script.kernel;

public class Literal implements Gettable {

    final Object value;

    public Literal(Object value) {
        this.value = value;
    }

    public Object get() {
        return value;
    }

    @Override
    public String toString() {
        if (value instanceof String)
            return "\"" + value + "\"";
        return String.valueOf(value);
    }
}
