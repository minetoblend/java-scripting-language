package com.minetoblend.script.kernel;

import org.reflections.util.ClasspathHelper;

import java.util.List;

public class ImportPath {
    private String packagePath;
    private String className;

    public ImportPath(String packagePath, String className) {
        this.packagePath = packagePath;
        this.className = className;
    }

    public ImportPath(String path) {
        int index = path.lastIndexOf('.');
        this.packagePath = path.substring(0,index);
        this.className = path.substring(index+1);
    }

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return packagePath + "." + className;
    }

    public Class getReferencedClass() {
        try {
            return Class.forName(this.toString());
        } catch (ClassNotFoundException ignore) {
            return null;
        }
    }
}
