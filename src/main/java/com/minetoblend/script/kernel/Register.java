package com.minetoblend.script.kernel;

public class Register implements Gettable<Object>, Settable<Object> {

    final String name;
    Object value;

    Register(String name) {
        this.name = name;
        value = 0;
    }

    public void set(Object val) {
        if (!(val instanceof Gettable))
            val = new Literal(val);
        value = val;
    }

    public Object get() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    boolean locked = false;

    public void lock(){
        locked = true;
    }

    public void unlock(){
        locked = false;
    }

    public boolean isLocked() {
        return locked;
    }
}