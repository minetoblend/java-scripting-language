package com.minetoblend.script.kernel;

import java.util.*;

public class Block {

    public Register eax = new Register("eax");
    int offset = 0;
    Block parent;
    Object self;
    Stack<Object> stack = new Stack<>();
    Map<String, Integer> varLocations = new HashMap<>();
    List<ScriptInstruction> statements = new ArrayList<>();


    public Block(Block parent, Object self) {
        this.parent = parent;
        this.self = self;
        if (self == null && parent != null)
            self = parent.self;
    }

    public StackLocation put(String name, Object o) {
        int index = stack.size();
        stack.push(o);
        varLocations.put(name, index);

        return new StackLocation(this, index);
    }

    public void push(Object o) {
        stack.push(o);
    }

    public Object pop() {
        return stack.pop();
    }

    public void push(String name) {
        varLocations.put(name, -1);
    }

    @Override
    public String toString() {

        return super.toString();

    }

    public StackLocation getStackLocation(String name) {
        int location = varLocations.getOrDefault(name, -2);
        if (location >= 0) {
            return new StackLocation(this, location);
        } else if (location == -1)
            throw new IllegalStateException("variable '" + name + "' has not been initialized yet");

        if (parent != null)
            return parent.getStackLocation(name);
        throw new IllegalArgumentException("variable '" + name + "' does not exist");
    }

    public void execute() {
        stack.clear();
        for (int i = 0; i < statements.size(); i++) {
            ScriptInstruction instruction = statements.get(i);
            System.out.println(instruction);
            instruction.execute(this);
        }
        System.out.println("\n\nExecution Results: \n");
        System.out.println("eax = " + eax.get());
        System.out.println("stack: ");
        printStack();
    }

    private void printStack() {
        if(parent != null)
            parent.printStack();
        for (int i = 0; i < stack.size(); i++) {
            Object o = stack.get(i);
            System.out.printf("%06x    %s", i, o);
        }
    }

    public void addStatement(ScriptInstruction i) {
        this.statements.add(i);
    }
}
