package com.minetoblend.script.kernel;

public abstract class ScriptInstruction {

    public int line;
    public int position;

    public abstract void execute(Block block);

    public static String formatLiteral(Object o) {
        if (o instanceof String)
            return "\"" + o + "\"";
        else
            return String.valueOf(o);
    }

}
