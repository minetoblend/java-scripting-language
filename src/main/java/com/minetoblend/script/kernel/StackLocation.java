package com.minetoblend.script.kernel;

import java.util.Set;
import java.util.Stack;

public class StackLocation implements Gettable, Settable {
    Block block;
    Stack stack;
    int location;

    public StackLocation(Block block, int location) {
        this(block, block.stack, location);
    }

    public StackLocation(Block block, Stack stack, int location) {
        this.block = block;
        this.stack = stack;
        this.location = location;
    }

    @Override
    public String toString() {
        if (location == stack.size() - 1)
            return "[esp]";
        return String.format("[esp - %d]", (stack.size() - location - 1));
    }

    /*public String toString() {
        return String.format("[curBlock + %s]", location);
    }*/

    @Override
    public Object get() {
        return stack.get(location);
    }

    @Override
    public void set(Object value) {
        stack.set(location, value);
    }
}
