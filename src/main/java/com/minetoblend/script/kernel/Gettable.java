package com.minetoblend.script.kernel;

public interface Gettable<T> {

    T get();

}
