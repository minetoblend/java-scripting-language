package com.minetoblend.script.kernel.instructions;

import com.minetoblend.script.kernel.Block;
import com.minetoblend.script.kernel.ScriptInstruction;
import com.minetoblend.script.kernel.ScriptMethod;
import com.minetoblend.script.kernel.StackLocation;

import java.lang.reflect.Method;

public class Call extends ScriptInstruction {
    StackLocation location;

    public Call(StackLocation stackLocation) {
        location = stackLocation;
    }

    @Override
    public void execute(Block block) {
        Object o = location.get();
        if (o instanceof Method) {
            System.out.println("native method");
        } else if (o instanceof ScriptMethod) {
            System.out.println("script method");
        } else
            throw new IllegalArgumentException(String.format("Error in %d,%d", line, position));
    }

    @Override
    public String toString() {
        return String.format("call %s", location);
    }
}
