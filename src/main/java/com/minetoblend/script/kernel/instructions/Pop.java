package com.minetoblend.script.kernel.instructions;

import com.minetoblend.script.kernel.Block;
import com.minetoblend.script.kernel.Register;
import com.minetoblend.script.kernel.ScriptInstruction;

public class Pop extends ScriptInstruction {

    Register register;

    public Pop(Register register) {
        this.register = register;
    }

    @Override
    public void execute(Block block) {
        register.set(block.pop());
    }


}
