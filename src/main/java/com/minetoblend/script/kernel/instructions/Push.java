package com.minetoblend.script.kernel.instructions;

import com.minetoblend.script.kernel.Block;
import com.minetoblend.script.kernel.ScriptInstruction;
import com.minetoblend.script.kernel.StackLocation;

public class Push extends ScriptInstruction {

    public StackLocation location;

    public Push(StackLocation location) {
        this.location = location;
    }


    @Override
    public void execute(Block block) {

    }

    @Override
    public String toString() {
        return String.format("push [%s]", location);
    }
}
