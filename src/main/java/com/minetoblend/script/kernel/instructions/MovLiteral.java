package com.minetoblend.script.kernel.instructions;

import com.minetoblend.script.kernel.Block;
import com.minetoblend.script.kernel.ScriptInstruction;
import com.minetoblend.script.kernel.Settable;
import com.minetoblend.script.kernel.StackLocation;

public class MovLiteral extends ScriptInstruction {

    Settable location;
    Object value;

    public MovLiteral(Settable location, Object value) {
        this.location = location;
        this.value = value;
    }

    @Override
    public void execute(Block block) {
        location.set(value);
    }

    @Override
    public String toString() {
        return String.format("mov %s %s", location, formatLiteral(value));
    }
}
