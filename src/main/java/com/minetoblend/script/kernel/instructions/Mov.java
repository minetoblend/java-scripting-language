package com.minetoblend.script.kernel.instructions;

import com.minetoblend.script.kernel.Block;
import com.minetoblend.script.kernel.Gettable;
import com.minetoblend.script.kernel.ScriptInstruction;
import com.minetoblend.script.kernel.StackLocation;

public class Mov extends ScriptInstruction {
    private final StackLocation location;
    private final Gettable value;

    public Mov(StackLocation location, Gettable value) {
        this.location = location;
        this.value = value;
    }

    @Override
    public void execute(Block block) {
        location.set(value.get());
    }

    public String toString() {
        return String.format("mov %s %s", location, value);
    }
}
