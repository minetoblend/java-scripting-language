package com.minetoblend.script.kernel.instructions;

import com.minetoblend.script.kernel.Block;
import com.minetoblend.script.kernel.Gettable;
import com.minetoblend.script.kernel.Literal;
import com.minetoblend.script.kernel.ScriptInstruction;

import java.util.List;

public class Add extends ScriptInstruction {


    public Gettable<Object> a;
    public Gettable<Object> b;

    public Add(Gettable<Object> a, Gettable<Object> b) {
        this.a = a;
        this.b = b;
    }

    public static Object add(Object a, Object b) {
        if (a instanceof Literal)
            a = ((Literal) a).get();
        if (b instanceof Literal)
            b = ((Literal) b).get();

        if (a instanceof String || b instanceof String) {

            return String.valueOf(a) + String.valueOf(b);
        }
        return null;
    }

    @Override
    public void execute(Block block) {
        Object a = this.a.get();
        Object b = this.b.get();

        block.eax.set(add(a, b));
    }

    @Override
    public String toString() {
        return String.format("add %s, %s", a, b);
    }
}
