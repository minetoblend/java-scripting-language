package com.minetoblend.script.kernel.instructions;

import com.minetoblend.script.kernel.Block;
import com.minetoblend.script.kernel.ScriptInstruction;

public class PushLiteral extends ScriptInstruction {
    private final Object value;

    public PushLiteral(Object value) {
        this.value = value;
    }

    @Override
    public void execute(Block block) {
        block.push(value);
    }

    @Override
    public String toString() {
        return String.format("push %s", formatLiteral(value));
    }


}
