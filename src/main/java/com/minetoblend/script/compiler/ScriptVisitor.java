package com.minetoblend.script.compiler;

import com.minetoblend.script.kernel.*;
import com.minetoblend.script.kernel.instructions.*;
import com.minetoblend.script.parser.ScriptBaseVisitor;
import com.minetoblend.script.parser.ScriptParser;
import org.antlr.v4.runtime.Token;

public class ScriptVisitor extends ScriptBaseVisitor<Object> {

    ScriptCompiler compiler;

    Block currentBlock;

    public ScriptVisitor(ScriptCompiler compiler) {
        this.compiler = compiler;
    }

    @Override
    public Object visitImportStatement(ScriptParser.ImportStatementContext ctx) {
        ImportPath path = new ImportPath(ctx.classPath().getText());
        compiler.imports.put(path.getClassName(), path.getReferencedClass());
        return null;
    }

    @Override
    public Object visitBlock(ScriptParser.BlockContext ctx) {
        Block block = new Block(null, null);
        currentBlock = block;
        super.visitBlock(ctx);
        block.execute();
        return currentBlock;
    }

    @Override
    public Object visitStatement(ScriptParser.StatementContext ctx) {
        Object o = visitChildren(ctx);
        {

            ScriptInstruction s = (ScriptInstruction) o;
            Token token = ctx.getStart();
            s.line = token.getLine();
            s.position = token.getCharPositionInLine();
            currentBlock.addStatement(s);
        }
        return o;
    }

    @Override
    public Object visitSimpleInvokation(ScriptParser.SimpleInvokationContext ctx) {
        String methodName = ctx.methodName().getText();

        StackLocation l = currentBlock.getStackLocation(methodName);

        Call call = new Call(l);


        return call;
    }

    @Override
    public Object visitVarDefNoInit(ScriptParser.VarDefNoInitContext ctx) {
        String varName = ctx.Identifier().getText();
        currentBlock.push(varName);

        return super.visitVarDefNoInit(ctx);
    }

    @Override
    public Object visitVarDef(ScriptParser.VarDefContext ctx) {
        String varName = ctx.Identifier().getText();
        Object value = visitExpression(ctx.expression());

        currentBlock.put(varName, null);

        if (value instanceof StackLocation) {
            return new Push((StackLocation) value);
        } else {
            return new PushLiteral(value);
        }
    }


    @Override
    public Object visitStringLiteral(ScriptParser.StringLiteralContext ctx) {
        String text = ctx.getText();
        return new Literal(text.substring(1, text.length() - 1));
    }

    @Override
    public Object visitVariableAssignment(ScriptParser.VariableAssignmentContext ctx) {
        String varName = ctx.Identifier().getText();

        StackLocation location = currentBlock.getStackLocation(varName);
        Object value = visitExpression(ctx.expression());
        if (value instanceof Gettable) {
            return new Mov(location, (Gettable) value);
        } else {
            return new MovLiteral(location, value);
        }
    }

    @Override
    public Object visitAddition(ScriptParser.AdditionContext ctx) {

        if (ctx.b == null)
            return super.visitAddition(ctx);

        currentBlock.eax.lock();

        Object a = visit(ctx.a);
        Object b = visit(ctx.b);

        if(a instanceof Literal && b instanceof Literal){

            MovLiteral literal = new MovLiteral(currentBlock.eax, new Literal(Add.add(a,b)));

            currentBlock.addStatement(literal);
            return currentBlock.eax;
        }

        currentBlock.eax.unlock();


        if (a instanceof Gettable && b instanceof Gettable) {
            currentBlock.addStatement(new Add((Gettable) a, (Gettable) b));
            return currentBlock.eax;
        }
        return null;
    }
}
