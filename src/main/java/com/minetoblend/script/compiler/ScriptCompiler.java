package com.minetoblend.script.compiler;

import com.minetoblend.script.Script;
import com.minetoblend.script.kernel.ImportPath;
import com.minetoblend.script.parser.ScriptLexer;
import com.minetoblend.script.parser.ScriptParser;
import com.minetoblend.script.parser.ScriptParser.CompilationUnitContext;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

import java.util.HashMap;
import java.util.Map;

public class ScriptCompiler {

    ScriptVisitor scriptVisitor = new ScriptVisitor(this);


    Map<String, Class> imports = new HashMap<>();

    public void compile(String code, Script script) {
        script.clear();

        CharStream charStream = CharStreams.fromString(code);

        ScriptLexer lexer = new ScriptLexer(charStream);
        TokenStream stream = new CommonTokenStream(lexer);
        ScriptParser parser = new ScriptParser(stream);
        CompilationUnitContext context = parser.compilationUnit();

        scriptVisitor.visitCompilationUnit(context);
    }

    public void addImport(ImportPath path) {
        Class c = path.getReferencedClass();
        imports.put(path.getClassName(), c);
    }


}
